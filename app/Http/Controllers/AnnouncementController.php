<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use Illuminate\Http\Request;

class AnnouncementController extends Controller
{
    public function create(){
        return view('announcement.create');
    }

    public function show(Announcement $announcement) {
        return view ('announcement.show', compact('announcement'));
    }
    
    public function index(){
        $announcements = Announcement::where('is_accepted', true)->orderBy('created_at', 'DESC')->get();
        return view('announcement.index', compact('announcements'));
    }
}