<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function show(Category $category){
         $announcements = $category->announcements()->where('is_accepted', true)->get();
         return view('category.show', compact('category', 'announcements'));
    
    }

}
