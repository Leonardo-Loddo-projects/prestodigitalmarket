<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function homePage(){
    $announcements = Announcement::where('is_accepted', true)->orderBy('created_at', 'DESC')->take(3)->get();   
    return view('welcome', compact('announcements'));
    }

    public function team(){
         return view('creator.team');
    }

    public function searchAnnouncements(Request $request){
        $announcements = Announcement::search($request->searched)->where('is_accepted', true)->paginate(10);
        return view('announcement.index', compact('announcements'));
    }
}
