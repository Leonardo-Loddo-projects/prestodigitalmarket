<?php

namespace App\Http\Controllers;

use App\Mail\RevisorMail;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class RevisorController extends Controller
{
    public function index(){
        $announcement_to_check = Announcement::where ('is_accepted', null)->first();
        return view('revisor.index', compact('announcement_to_check'));
    }

    public function acceptAnnouncement(Announcement $announcement){
        $announcement->setAccepted(true);
        return redirect()->back()->with('message', 'Annuncio Accettato');
        }

    public function rejectAnnouncement(Announcement $announcement){
        $announcement->setAccepted(false);
        return redirect()->back()->with('message', 'Annuncio Rifiutato');
    }

    public function become() {
        Mail::to('admin@presto.it')->send(new RevisorMail(Auth::user()));
        return redirect('/')->with('message', 'Richiesta di diventare un revisore inviata!');
    }

    public function make(User $user) {
        Artisan::call('presto:makeUserRevisor', ["email"=>$user->email]);
        return redirect('/')->with('message', 'Congratulazioni! Sei appena diventato un revisore.');
    }

    public function restoreAnnouncement() {
        $announcements = Announcement::where('is_accepted', 0)->get();

        return view('revisor.restore', compact('announcements'));
    }
}
