<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Support\Facades\Auth;

class CreateAnnouncement extends Component
{
    public $title,$subtitle,$body,$price,$category;

    protected $rules = [
        'title' => 'required|min:5',
        'subtitle' => 'required|min:10',
        'body' => 'required|min:20',
        'price' => 'required|numeric',
        'category' => 'required',
    ];

    public function store(){
        $category = Category::find($this->category);
        $announcement = $category->announcements()->create([
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'body' => $this->body,
            'price' => $this->price,
        ]);
        Auth::user()->announcements()->save($announcement);
        session()->flash('announcementSuccess', 'Il tuo annuncio è stato pubblicato con successo!');
        $this->reset();
    }
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
    public function render()
    {
        return view('livewire.create-announcement');
    }
}
