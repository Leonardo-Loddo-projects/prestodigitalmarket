<?php

use App\Models\Category;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('icon');
            $table->timestamps();
        });

        $categories= [
            ['NFT', 'fa-solid fa-file-image'],
            ['APP', 'fa-solid fa-mobile-screen-button'], 
            ['Crypto', 'fa-solid fa-bitcoin-sign'], 
            ['Videogame KEY', 'fa-solid fa-gamepad'], 
            ['SOFTWARE', 'fa-solid fa-download'], 
            ['OS', 'fa-solid fa-laptop-file'], 
            ['Sample Packs', 'fa-solid fa-drum'], 
            ['PlugIns', 'fa-solid fa-plug-circle-check'], 
            ['MODS', 'fa-solid fa-hat-wizard'],
            ['E-Books', 'fa-solid fa-book'],  
            ];

            foreach($categories as $category) {
                Category::create([
                    'name' => $category[0],
                    'icon' => $category[1],
                ]);
            }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
};
