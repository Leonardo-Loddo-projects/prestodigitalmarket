<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <h2 class="d-flex justify-content-center my-5 text-light">Sezione Inserimento Annunci</h2>
                @livewire('create-announcement')
            </div>
        </div>
    </div>
</x-layout>