<x-layout>
    <x-slot name="title">Tutti gli Annunci</x-slot>

    {{-- SEZIONE Tutti gli Annunci --}}
    <div class="class container">
        <h2 class="text-light">Tutti gli Annunci</h2>
        <div class="row d-flex justify-content-around">
          @if (count($announcements)>0)
                    {{-- generazione card annunci --}}
                    @foreach($announcements as $announcement)
                    <div class="card glassCard col-12  col-md-3 m-1">
                      <h5 class="card-title text-white p-2">
                        <a href="{{route('announcement.show', compact('announcement'))}}" class="text-light user-avatar text-decoration-none rounded bordered ">
                          <img class="rounded imgLoghi rounded-circle me-2" alt="Image" src="https://picsum.photos/1000">
                          {{$announcement->title}}
                        </a>
                      </h5> 
                        <div class="card-body">
                          <h6 class="card-subtitle mb-2 text-warning">{{$announcement->subtitle}}</h6>
                          <p class=" text-white">{{$announcement->created_at->format('d/m/Y')}}</p>
                          <a href="" class="text-decoration-none text-info">{{$announcement->category->name}}</a>
                          <p class="card-text text-white">{{$announcement->price}} &euro;</p>
                          <a id="btn-announcementShow" href="{{route('announcement.show', compact('announcement'))}}" class="card-link text-white btn btn-warning">Vedi annuncio</a>
                        </div>
                    </div>    
                    @endforeach
              @else
              <div class="d-flex justify-content-center text-light">
                <h2>Nessun annuncio pubblicato</h2>
                <a id="btn-one" class="btn btn-info ms-3" href="{{route('announcement.create')}}">Scrivine uno</a>
              </div>
          @endif
        </div>
      </div> 
      {{-- !SEZIONE CARD RECENTI --}} 
      
</x-layout>