<x-layout>
  <x-slot name="title">{{$announcement->title}}</x-slot>
  <div class="container my-0">
    <div class="row justify-content-center">
      <div class="col-12 col-md-8 glassInfo p-5 text-center">
        
        {{-- carousel start --}}
        
        <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
          <div class="carousel-inner">
            <div>
              <div class="carousel-item active">
                <img src="https://picsum.photos/1000/600" class="carousel-image" alt="...">
              </div>
              <div class="carousel-item">
                <img src="https://picsum.photos/999/600" class="carousel-image" alt="...">
              </div>
              <div class="carousel-item">
                <img src="https://picsum.photos/998/600" class="carousel-image" alt="...">
              </div>
            </div>
          </div>
          <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
        
        {{-- carousel end --}}
        
        <hr>
        <p class="my-5 text-light card-text">{{$announcement->title}}</p>
        <p class="my-5 text-light">{{$announcement->subtitle}}</p>
        <p class="my-5 text-light card-text">{{$announcement->price}} &euro;</p>
        <p class="my-5 text-light">{{$announcement->body}}</p>
        <a class="btn btn-success" href="{{route('category.show', ['category'=> $announcement->category->id])}}">Torna Indietro<span></span></a>
      </div>
    </div>
  </div>
</x-layout>