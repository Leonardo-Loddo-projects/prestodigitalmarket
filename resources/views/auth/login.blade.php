<x-layout>
  <h2 class="d-flex justify-content-center text-light mt-2">Accedi</h2>
        <div class="container my-4 glassCard">
            <div class="row justify-content-center">
                <div class="col-12">
                    <form  class="p-5" method="POST" action="{{route('login')}}">
                        @csrf
                        <x-errors />
                        <div class="mb-3 text-white">
                          <label for="email" class="form-label" placeholder="Lorenzo">Indirizzo Email</label>
                          <input type="email" name="email" class="form-control" id="email" value="{{old('email')}}">
                        </div>
                          <div class="mb-3 text-white">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" name="password" class="form-control" id="password">
                          </div>
                          <div class="mb-3 form-check text-white">
                            <input type="checkbox" class="form-check-input" id="remember" name="remember">
                            <label class="form-check-label" for="remember">Ricordami</label>
                          </div>
                        <button type="submit" class="btn btn-success">Accedi</button>
                        <a class="btn btn-info text-light ms-3" href="{{route('register')}}">Registrati</a>
                      </form>
                </div>
            </div>
        </div>  
</x-layout>