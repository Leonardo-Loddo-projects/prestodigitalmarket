<x-layout>
  <h2 class="d-flex justify-content-center text-light mt-2">Registrati</h2>
    <div class="container my-4 glassCard">
        <div class="row justify-content-center">
            <div class="col-12">
                <form  class="p-5 " method="POST" action="{{route('register')}}">
                    @csrf
                    <x-errors />
                    <div class="mb-3 text-white">
                      <label for="name" class="form-label" placeholder="Lorenzo">Nome Utente</label>
                      <input type="text" name="name" class="form-control" id="name" value="{{old('name')}}">
                    </div>
                    <div class="mb-3 text-white">
                        <label for="email" class="form-label" placeholder="lorenzo@gmail.com">Indirizzo E-mail</label>
                        <input type="email" name="email" class="form-control" id="email" value="{{old('email')}}">
                      </div>
                      <div class="mb-3 text-white">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" name="password" class="form-control" id="password">
                      </div>
                      <div class="mb-3 text-white">
                        <label for="password_confirmation" class="form-label">Conferma Password</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
                      </div>
                    <button  type="submit" class="btn btn-success">Registrati</button>
                    <a class="btn btn-info text-light ms-3" href="{{route('login')}}">Ho già un Profilo</a>
                  </form>
            </div>
        </div>
    </div>
</x-layout>