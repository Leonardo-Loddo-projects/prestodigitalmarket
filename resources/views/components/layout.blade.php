<!doctype html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- IMPOSTA IL TITOLO DINAMICAMENTE --}}
    <title>{{$title ?? 'Presto'}}</title>
    {{-- COLLEGA I FOGLI DI STILE E I FILE JS --}}
    {{-- SWIPER CSS --}}
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css"
    />
    @livewireStyles
    @vite(['resources/css/app.css', 'resources/js/app.js'])
  </head>
  <body>
    <div class="video-bg">
      <video width="320" height="240" autoplay loop muted>
       <source src="https://assets.codepen.io/3364143/7btrrd.mp4" type="video/mp4">
     </video>
    </div>
    <x-navbar/>
    <x-sidebar>
      <div class="glassBg">
        {{$slot}}
      </div>
    </x-sidebar>
    {{-- SWIPER JS --}}
    <script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
    @livewireScripts
  </body>
</html>