<nav class="navbar navbar-expand-lg navbar-dark glassMenu">
   <div class="container-fluid">
      <a class="navbar-brand" href="{{route('homePage')}}">PRESTO.it</a>


          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              {{-- ELEMENTI NAVBAR  --}}
              {{-- A SECONDA DELLA ROUTE I LINK DIVENTANO ACTIVE DINAMICAMENTE --}}
              <a class="nav-link home me-4 ms-3 mb-3 {{Route::is('homePage') ? 'active' : ''}}" aria-current="page" href="{{Route('homePage')}}">Home</a>
              @auth
                <a class="nav-link active me-4 mt-2">Ciao {{Auth::user()->name}} !</a>
              @endauth

              {{-- <form action="{{route('announcements.search')}}" method="GET" class="d-flex">
              <input type="search" class="form-control" name="searched" placeholder="Search                     &#128269" aria-label="Search">
              <button class="btn" type="submit"><i class="bi bi-search text-light"></i></button>
              </form> --}}

              {{-- FORM PER RICERCARE TESTO PER CATEGORIA, PREZZO E NOME / FULL-TEXT  --}}
              <form action="{{route('announcements.search')}}" method="GET" class="d-flex">
                <input type="search" class="form-control" name="searched" placeholder="Ricerca">
                <button class="btn bg-dark btn-outline-light">Cerca</button>
              </form>
          </div>
        </div>
   </div>
  </nav>