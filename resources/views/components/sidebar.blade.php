<div class="container-fluid position-absolute"> 
     <div class="row flex-nowrap">
        <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 glassMenu">
            <div class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
                <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start" id="menu">
                    {{-- Annunci --}}
                    <li class="nav-item">
                        <a href="{{route('announcement.index')}}" class="nav-link align-middle px-0 text-light">
                            <i class="bi bi-megaphone-fill"></i><h5 class="ms-1 d-none d-md-inline"> Annunci</h5>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('announcement.index')}}" class="nav-link align-middle px-0 {{Route::is('announcement.index') ? 'text-light' : 'text-info'}}">
                            <i class="bi bi-border-all d-none d-md-inline"></i><span class="ms-1 d-none d-md-inline"> Tutti gli annunci</span>
                        </a>
                    </li>
                    @auth
                    <li class="nav-item">
                        <a href="{{route('announcement.create')}}" class="nav-link align-middle px-0 {{Route::is('announcement.create') ? 'text-light' : 'text-info'}}">
                            <i class="bi bi-plus-square-fill d-none d-md-inline"></i><span class="ms-1 d-none d-md-inline"> Inserisci annuncio</span>
                        </a>
                    </li>
                    @endauth
                    {{-- categorie --}}
                    <li class="nav-item">
                        <a href="#submenu2" data-bs-toggle="collapse" class="nav-link align-middle px-0 text-light">
                            <i class="bi bi-tags-fill"></i> <h5 class="ms-1 d-none d-md-inline">Categorie</h5>
                        </a>
                        <ul class="collapse nav flex-column ms-1" id="submenu2" data-bs-parent="#menu">
                            @foreach($categories as $category)
                            <li class="nav-item">
                                <a href="{{route('category.show', compact('category'))}}" class="nav-link align-middle px-0 text-light {{Route::is('category.show') ? 'text-light' : 'text-muted'}}">
                                    <i class="{{$category->icon}}"></i><span class="ms-1 d-none d-md-inline">{{($category->name)}}</span>
                                </a>
                            </li>    
                            @endforeach
                        </ul>
                    </li>
                    {{-- lavora con noi --}}
                    <li class="nav-item">
                        <a href="@if (Auth::user() && Auth::user()->is_revisor) {{route('revisor.index')}} @else {{route('revisor.become')}} @endif " class="nav-link align-middle px-0 text-light">
                            <i class="bi bi-hammer"></i> <h5 class="ms-1 d-none d-md-inline">Carriera</h5>
                        </a>
                    </li>
                    @if (Auth::user() && Auth::user()->is_revisor)
                    <li class="nav-item">
                        <a href="{{route('revisor.index')}}" class="nav-link align-middle px-0 {{Route::is('revisor.index') ? 'text-light' : 'text-muted'}}">
                            <i class="bi bi-binoculars-fill d-none d-md-inline"></i><span class="ms-1 d-none d-md-inline"> Zona Revisore 
                                <span class="ms-2 mt-1 position-absolute start-90 translate-middle badge rounded-pill bg-danger">
                                    {{App\Models\Announcement::toBeRevisionedCount()}}
                                </span>
                            </span>
                        </a>
                    </li>
                    {{-- AGGIUNTA SEZIONE DIVENTA REVISORE NELLA WELCOME CON DINAMICITA' A 3 CLASSI --}}
                    {{-- @else --}}
                    {{-- <li class="nav-item">
                        <a href="{{route('revisor.become')}}" class="nav-link align-middle px-0 text-light text-muted">
                            <i class="bi bi-binoculars-fill d-none d-md-inline"></i><span class="ms-1 d-none d-md-inline"> Diventa un revisore</span>
                        </a>
                    </li> --}}
                    @endif
                    {{-- profilo --}}
                    <li class="nav-item">
                        @auth
                        <a href="#" class="nav-link align-middle px-0 text-light">
                        @else    
                        <a href="{{route('login')}}" class="nav-link align-middle px-0 text-light">
                        @endauth    
                            <i class="bi bi-person-badge"></i>@auth<h5 class="ms-1 d-none d-md-inline">{{Auth::user()->name}}</h5>@else<h5 class="ms-1 d-none d-md-inline">Profilo</h5>@endauth
                        </a>
                    </li>
                    @auth
                    <li class="nav-item">
                        <a href="#" class="nav-link align-middle px-0 text-light text-info" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();">
                            <i class="bi bi-box-arrow-left d-none d-md-inline"></i><span class="ms-1 d-none d-md-inline">Logout</span>
                        </a>
                        <form action="{{route('logout')}}" id="form-logout" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                    <li class="nav-item">
                      <a href="#" class="nav-link align-middle px-0 {{Route::is('#') ? 'text-light' : 'text-muted'}}">
                          <i class="bi bi-border-all d-none d-md-inline"></i><span class="ms-1 d-none d-md-inline">I tuoi Annunci</span>
                      </a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a href="{{route('login')}}" class="nav-link align-middle px-0 {{Route::is('login') ? 'text-light' : 'text-info'}}">
                            <i class="bi bi-box-arrow-in-right d-none d-md-inline"></i><span class="ms-1 d-none d-md-inline">Login</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('register')}}" class="nav-link align-middle px-0 {{Route::is('register') ? 'text-light' : 'text-info'}}">
                            <i class="bi bi-card-checklist d-none d-md-inline"></i><span class="ms-1 d-none d-md-inline">Registrati</span>
                        </a>
                    </li>
                    @endauth

                    {{-- crediti --}}
                    <li class="nav-item">
                        <a href="#submenu3" data-bs-toggle="collapse" class="nav-link align-middle px-0 text-light">
                            <i class="bi bi-tags-fill"></i> <h5 class="ms-1 d-none d-md-inline">Crediti</h5>
                        </a>
                        <ul class="collapse nav flex-column ms-1" id="submenu3" data-bs-parent="#menu">
                            <li class="nav-item my-3">                                
                                    <span class="ms-1 d-none d-md-inline text-muted">Pasquale Cordisco</span>
                                <li class="nav-item mb-3">
                                        <span class="ms-1 d-none d-md-inline text-muted">Danny Figliuzzi</span>
                                </li>
                            <li class="nav-item mb-3">
                                    <span class="ms-1 d-none d-md-inline text-muted">Leonardo Loddo</span>
                            </li>
                            <li class="nav-item mb-3">
                                    <span class="ms-1 d-none d-md-inline text-muted">Giorgio Valerio Cieri</span>
                            </li>
                            <li class="nav-item mb-3">
                                    <span class="ms-1 d-none d-md-inline text-muted">Lorenzo Demontis</span>
                            </li>
                            </li>
                            <li class="nav-item mt-4">
                                <a href="{{route('creator.team')}}" class="nav-link align-middle px-0 text-warning">
                                    <span class="ms-1 d-none d-md-inline">CHI SIAMO</span>
                                </a>
                            </li>
                        </ul>
                      </li>
                    </ul>
                </div>
            </div>
            <div class="col py-3">
                {{$slot}}
            </div>
        </div>
    </div>
</div>