<x-layout>


    <div class="card glassCard ">
        <div class="card-header d-flex justify-content-center text-light">
          TEAM
        </div>
        <div class="card-body text-light">

            {{-- Pasquale --}}

            <li class="list-group-item bg-transparent border-bottom py-3 px-0">
                <div class="row align-items-center">
                    <div class="col-auto">
                        <a class="user-avatar rounded bordered">
                            <img class="rounded imgLoghi rounded-circle" alt="Image" src="/media/logoPasquale.jpg">
                        </a>
                    </div>
                    <div class="col-auto px-0">
                        <h4 class="fs-6 mb-0">
                            Pasquale Rosario Cordisco
                        </h4>
                        <span class="small text-warning">Full-Stack Web Developer Junior</span>
                    </div>
                    <div class="col text-end">
                        <a class="me-3 text-decoration-none" href="https://www.instagram.com/cordiscopasquale/" target="blank">
                            <img src="/media/Instagram.ico" alt="">
                        </a>
                        <a href="https://www.linkedin.com/in/pasquale-rosario-cordisco-2b7988243/" target="blank">
                            <img src="/media/LinkedIN.ico" alt="">
                        </a>
                    </div>
                </div>
            </li>

            {{-- Danny --}}

            <li class="list-group-item bg-transparent border-bottom py-3 px-0">
                <div class="row align-items-center">
                    <div class="col-auto">
                        <a class="user-avatar rounded bordered">
                            <img class="rounded imgLoghi rounded-circle" alt="Image" src="/media/logoDanny.jpeg">
                        </a>
                    </div>
                    <div class="col-auto px-0">
                        <h4 class="fs-6 mb-0">
                            Danny Figliuzzi
                        </h4>
                        <span class="small text-warning">Full-Stack Web Developer Junior</span>
                    </div>
                    <div class="col text-end">
                        <a class="me-3 text-decoration-none" href="https://www.instagram.com/_d.a.n.n.y_01/" target="blank">
                            <img src="/media/Instagram.ico" alt="">
                        </a>
                        <a href="https://www.linkedin.com/in/danny-figliuzzi-junior-web-developer/" target="blank">
                            <img src="/media/LinkedIN.ico" alt="">
                        </a>
                    </div>
                </div>
            </li>

            {{-- Leonardo --}}

            <li class="list-group-item bg-transparent border-bottom py-3 px-0">
                <div class="row align-items-center">
                    <div class="col-auto">
                        <a class="user-avatar rounded bordered">
                            <img class="rounded imgLoghi rounded-circle" alt="Image" src="/media/logoLeo.jpg">
                        </a>
                    </div>
                    <div class="col-auto px-0">
                        <h4 class="fs-6 mb-0">
                            Leonardo Loddo
                        </h4>
                        <span class="small text-warning">Full-Stack Web Developer Junior</span>
                    </div>
                    <div class="col text-end">
                        <a class="me-3 text-decoration-none" href="https://www.instagram.com/loddo.sf/" target="blank">
                            <img src="/media/Instagram.ico" alt="">
                        </a>
                        <a href="https://www.linkedin.com/in/leonardo-loddo-full-stack-web/" target="blank">
                            <img src="/media/LinkedIN.ico" alt="">
                        </a>
                    </div>
                </div>
            </li>

            {{-- Giorgio --}}

            <li class="list-group-item bg-transparent border-bottom py-3 px-0">
                <div class="row align-items-center">
                    <div class="col-auto">
                        <a class="user-avatar rounded bordered">
                            <img class="rounded imgLoghi rounded-circle" alt="Image" src="/media/logoGiorgio.jpg">
                        </a>
                    </div>
                    <div class="col-auto px-0">
                        <h4 class="fs-6 mb-0">
                            Giorgio Valerio Cieri
                        </h4>
                        <span class="small text-warning">Full-Stack Web Developer Junior</span>
                    </div>
                    <div class="col text-end">
                        <a class="me-3 text-decoration-none" href="https://www.instagram.com/gozer_il_gozeriano/" target="blank">
                            <img src="/media/Instagram.ico" alt="">
                        </a>
                        <a href="https://www.linkedin.com/in/giorgio-valerio-cieri-webdeveloper/" target="blank">
                            <img src="/media/LinkedIN.ico" alt="">
                        </a>
                    </div>
                </div>
            </li>

            {{-- Lorenzo --}}

            <li class="list-group-item bg-transparent border-bottom py-3 px-0">
                <div class="row align-items-center">
                    <div class="col-auto">
                        <a target="blank" class="user-avatar rounded bordered">
                            <img class="rounded imgLoghi rounded-circle" alt="Image" src="/media/logoLorenzo.jpg">
                        </a>
                    </div>
                    <div class="col-auto px-0">
                        <h4 class="fs-6 mb-0">
                            Lorenzo Demontis
                        </h4>
                        <span class="small text-warning">Full-Stack Web Developer Junior</span>
                    </div>
                    <div class="col text-end">
                        <a class="me-3 text-decoration-none" href="https://www.instagram.com/lorenzoildemo/" target="blank">
                            <img src="/media/Instagram.ico" alt="">
                        </a>
                        <a href="https://www.linkedin.com/in/lorenzo-demontis-junior-web-developer/" target="blank">
                            <img src="/media/LinkedIN.ico" alt="">
                        </a>
                    </div>
                </div>
            </li>

        </div>
      </div>
</x-layout>