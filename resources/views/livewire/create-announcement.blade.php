    <form id="form-announcement" class="m-5 p-5 shadow text-light glassCard" wire:submit.prevent="store">
          @csrf
          @if (session()->has('announcementSuccess'))
                  <div class="alert alert-success d-flex justify-content-center">
                      {{ session('announcementSuccess') }}
                  </div>
          @endif
          <x-errors />
          <div class="mb-3 ">
            <label for="title" class="form-label" >Titolo Annuncio</label>
            <input type="text" wire:model.lazy="title" class="form-control" id="title" value="{{old('title')}}">
          </div>
          <div class="mb-3 ">
              <label for="subtitle" class="form-label" >Sottotitolo Annuncio</label>
              <input type="text" wire:model.lazy="subtitle" class="form-control" id="subtitle" value="{{old('subtitle')}}">
            </div>
            <div class="mb-3 ">
              <label for="price" class="form-label" >Prezzo</label>
              <input type="number" min="0" step="0.01" wire:model.lazy="price" class="form-control" id="price" value="{{old('price')}}">
            </div>

            <div class="mb-3">
              <label for="category">Categoria</label>
              <select wire:model.defer="category" id="category" class="form-control">
                <option value="">Scegli la categoria</option>
                @foreach ($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
              </select>

            </div>

            <div class="mb-3 ">
              <label for="body" class="form-label">Descrizione Articolo</label>
              <textarea type="text" wire:model.lazy="body" class="form-control" id="body" rows="5" cols="7">{{old('body')}}</textarea>
            </div>
          <button id="btn-create" type="submit" class="btn btn-success mt-4">Inserisci Annuncio</button>
    </form>

