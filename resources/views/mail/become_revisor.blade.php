<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PRESTO.IT</title>
</head>
<body>
    <div>
        <h1 class="d-flex justify-content-center">Un utente ha richiesto di lavorare con noi!</h1>
        <h2 class="text-success">Ecco i suoi dati:</h2>
        <p class="text-primary">Nome: {{$user->name}}</p>
        <p class="text-primary">Email: {{$user->email}}</p>
        <p class="d-flex justify-content-center">Se vuoi renderlo revisore clicca qui:</p>
        <a class="btn btn-success" href="{{route('revisor.make', compact('user'))}}">Rendi revisore l'utente.</a>
    </div>
    
</body>
</html>