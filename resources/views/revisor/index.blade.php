<x-layout>
    <div class="class container-fluid p-5 shadow mb-4">
        <div class="row">
            <div class="class glassCard col-12 text-light p-5">
                <h1 class="d-flex justify-content-center">
                    {{$announcement_to_check ? 'Annunci da revisionare' : 'Non ci sono annunci da revisionare'}}
                </h1>
            </div>
        </div>
    </div>
    
    
    {{-- Messaggi per annuncio/rifiuato/accettato --}}
    @if(session('message'))
        <div class="alert alert-info m-1 d-flex justify-content-center">
            <p class="m-1">{{session('message')}}</p>
        </div>
    @endif
    
    @if($announcement_to_check)
    <div class="container my-0">
        <div class="row justify-content-center">
          <div class="col-12 col-md-8 glassInfo p-5 text-center">
            
            {{-- carousel start --}}
            
            <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
              <div class="carousel-inner">
                <div>
                  <div class="carousel-item active">
                    <img src="https://picsum.photos/1000/600" class="carousel-image" alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="https://picsum.photos/999/600" class="carousel-image" alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="https://picsum.photos/998/600" class="carousel-image" alt="...">
                  </div>
                </div>
              </div>
              <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
              </button>
              <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
              </button>
            </div>
            
            {{-- carousel end --}}
            
            <hr>
            <p class="my-5 text-light card-text">{{$announcement_to_check->title}}</p>
            <p class="my-5 text-light">{{$announcement_to_check->subtitle}}</p>
            <p class="my-5 text-light card-text">{{$announcement_to_check->price}} &euro;</p>
            <p class="my-5 text-light">{{$announcement_to_check->body}}</p>
            <a class="btn btn-info" href="{{route('category.show', ['category'=> $announcement_to_check->category->id])}}">Torna Indietro<span></span></a>
          </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-6">
                <form action="{{route('revisor.accept_announcement', ['announcement'=>$announcement_to_check])}}" method="POST">
                    @csrf
                    @method('PATCH')
                        <button type="submit" class="btn btn-success shadow">Accetta</button>
                </form>
            </div>
            <div class="col-12 col-md-6 text-end">
                <form action="{{route('revisor.reject_announcement', ['announcement'=>$announcement_to_check])}}" method="POST">
                    @csrf
                    @method('PATCH')
                        <button type="submit" class="btn btn-danger shadow">Rifiuta</button>
                </form>
        </div>
      </div>
    </div>
    @endif
    {{-- <div class="d-flex justify-content-center">
      <form action="{{route('revisor.restore')}}" method="GET">
        @csrf
        <button class="btn btn-warning">Rettifica annunci</button>
      </form>
    </div> --}}

</x-layout>