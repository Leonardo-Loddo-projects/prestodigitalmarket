<x-layout>
    <div class="glassCardtext text-info">
        <h2 class="d-flex justify-content-center">Non sei sicuro della tua ultima decisione presa?</h2>
        <h2 class="d-flex justify-content-center">Non c'è problema puoi rettificare gli articoli!</h2>
    </div>
    {{-- Contenitore con gli annunci da revisionare --}}
    <div class="card glassCard my-5">
        <div class="card-header text-light d-flex justify-content-center">
          Annunci
        </div>
        <div class="card-body">
            @foreach($announcements as $announcement)
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title">{{$announcement->title}}</h5>
                  <h6 class="card-subtitle mb-2 text-muted">{{$announcement->subtitle}}</h6>
                  <p class="card-text">{{$announcement->category}}</p>
                  <h4 class="card-link">{{$announcement->price}}</h4>
                  <h6 class="card-link">{{$announcement->body}}</h6>
                  <form action="{{route('revisor.accept_announcement', ['announcement' => $announcements])}}" method="POST">
                  @csrf
                  @method('PATCH')
                  <button class="btn btn-warning">Conferma Rettifica</button>
                  </form>
                </div>
              </div>
        </div>
        @endforeach
      </div>
</x-layout>