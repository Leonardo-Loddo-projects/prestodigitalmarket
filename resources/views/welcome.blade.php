<x-layout>
    {{-- DEFINISCO IL TITOLO --}}
    <x-slot name="title">PRESTO</x-slot>

     {{-- Messaggi  --}}
    @if(session('access.denied'))
    <div class="alert alert-danger">
      <p>{{session('access.denied')}}</p>
    </div>
    @endif

    @if(session('message'))
        <div class="alert alert-success m-1 d-flex justify-content-center">
            <p class="m-1">{{session('message')}}</p>
        </div>
    @endif

    {{-- CAROSELLO --}}
      <!-- Swiper -->
      <div class="container mb-3">
        <div class="row ">

          <div class="d-flex justify-content-center col-12 col-lg-4 col-md-3">
            <div class="swiper mySwiper">
              <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <img class="img-carosello" src="/media/nft1.gif" />
                </div>
                <div class="swiper-slide">
                  <img class="img-carosello" src="/media/nft2.gif" />
                </div>
                <div class="swiper-slide">
                  <img class="img-carosello" src="/medianft3.gif" />
                </div>
                <div class="swiper-slide">
                  <img class="img-carosello" src="/media/nft4.gif" />
                </div>
                <div class="swiper-slide">
                  <img class="img-carosello" src="/media/nft5.gif" />
                </div>
                <div class="swiper-slide">
                  <img class="img-carosello" src="/media/nft6.gif" />
                </div>
                <div class="swiper-slide">
                  <img class="img-carosello" src="/media/nft7.gif" />
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-9 col-lg-6 mt-3 text-light col-12">
            <h1>Esplora il nostro SHOP</h1>
            <p class="fs-4">Ti diamo il benvenuto nel Futuro</p>
            <p class="fs-4">Acquista o vendi Licenze e Software tramite i tuoi annunci!</p>
            <p class="fs-4">Trada NFT e Crypto con Milioni di utenti da tutto il Globo!</p>
          </div>

        </div>
      </div>
    {{-- FINE CAROSELLO --}}

    {{-- SEZIONE CARD RECENTI --}}
    <div class="container">
      <h2 class="text-light">Annunci recenti</h2>
      <div class="row d-flex justify-content-around">
        @if (count($announcements)>0)
                  {{-- generazione card annunci --}}
                  @foreach ($announcements as $announcement)
                  <div class="card glassCard col-12  col-md-4 m-1">
                    <h5 class="card-title text-white p-2">
                      <a href="{{route('announcement.show', compact('announcement'))}}" class="text-light user-avatar text-decoration-none rounded bordered ">
                        <img class="rounded imgLoghi rounded-circle me-2" alt="Image" src="https://picsum.photos/1000">
                        {{$announcement->title}}
                      </a>
                    </h5> 
                      <div class="card-body">
                        <h6 class="card-subtitle mb-2 text-warning">{{$announcement->subtitle}}</h6>
                        <p class=" text-white">{{$announcement->created_at->format('d/m/Y')}}</p>
                        <a href="{{route('category.show', ['category'=>$announcement->category->id])}}" class="text-decoration-none text-info">{{$announcement->category->name}}</a>
                        <p class="card-text text-white">{{$announcement->price}} &euro;</p>
                        <a id="btn-announcementShow" href="{{route('announcement.show', compact('announcement'))}}" class="card-link text-white btn btn-warning">Vedi annuncio</a>
                      </div>
                  </div>    
                  @endforeach
            @else
            <div class="d-flex justify-content-center text-light">
              <h2>Nessun annuncio pubblicato</h2>
              <a id="btn-one" class="btn btn-info ms-3" href="{{route('announcement.create')}}">Scrivine uno</a>
            </div>
        @endif
      </div>
    </div> 
    {{-- !SEZIONE CARD RECENTI --}} 

    <x-divider/>

    {{-- SEZIONE CATEGORIE --}}
    <div class="container">
      <h2 class="text-light d-flex justify-content-center pb-4">Categorie Popolari</h2>
      <div class="flex-nowrap d-flex justify-content-around mt-3">

        <div class="col-12 col-md-3">
          <a href="/1/show">
            <div class="flip-card">
              <div class="flip-card-inner">
                <div class="flip-card-front">
                  <img src="/media/NFT1.png" />
                  <h1 class="d-md-none text-light">NFT</h1>
                </div>
                <div class="flip-card-back">
                  <img src="/media/NFT.png" />
                  <h3>Entra nel mondo degli</h3>
                  <h1 class="text-nft">NFT</h1>
                </div>
              </div>
            </div>
          </div>
        </a>

        <div class="col-12 col-md-3">
          <a href="/3/show">
            <div class="flip-card">
              <div class="flip-card-inner">
                <div class="flip-card-front">
                  <img src="/media/Bitcoin.png" />
                </div>
                <div class="flip-card-back">
                  <img src="/media/Crypto2.png" />
                  <h3>Entra nel mondo</h3>
                  <h1 class="text-crypto">CRYPTO</h1>
                </div>
              </div>
            </div>
          </div>
        </a>

        <div class="col-12 col-md-3">
          <a href="/5/show">
            <div class="flip-card">
              <div class="flip-card-inner">
                <div class="flip-card-front">
                  <img src="/media/Software1.png" />
                </div>
                <div class="flip-card-back">
                  <img src="/media/Software.png" />
                  <h3>Entra nel mondo</h3>
                  <h1 class="text-software">Software</h1>
                </div>
              </div>
            </div>
          </div>
        </a>

        {{-- Generazione card category --}}
        {{-- @foreach ($categories as $category)
        <div class="card card-category justify-content-center ms-2 d-flex ms-5 glassCardCategory col-12 col-md-4 m-1">
          <img class="card-img-top" src="https://static.okx.com/cdn/nft/bea80c03-465d-4091-9459-ec39e4cd930c.jpg" alt="Card image cap">
          <div class="card-body text-center">
            <h5 class="text-light fs-6">{{$category->name}}</h5>
          </div>
        </div>
        @endforeach --}}
        
      </div>
    </div>
    {{-- !SEZIONE CATEGORIE --}}
    
    <x-divider/>

    {{-- SEZIONE LAVORA CON NOI --}}
    {{-- <div class="class container">
      <h2 class="text-light d-flex justify-content-center">Lavora con noi</h2>
      <div class="row justify-content-around">
      </div>
    </div> --}}

    {{-- SEZIONE LAVORA CON NOI + CONDIZIONE UTENTE VERIFICATO E GUEST--}}
    @if(Auth::user() && Auth::user()->is_revisor)
    <div class="card glassCard">
      <div class="card-headerWork text-light ms-4">
        Lavora con noi
      </div>
      <div class="card-body text-light">
        <img class="img-work" src="/media/LavoraConNoi.png" alt="">
        <h5 class="card-text text-light d-flex justify-content-center mt-3">Congratulazioni. Sei già un revisore!</h5>
        <h5 class="card-title text-light d-flex justify-content-center mt-3">{{Auth::user()->name}} lavora sodo e sii minuzioso durante la revisioni degli articoli </h5>
      </div>
    </div>
    @else
    <div class="card glassCard">
      <div class="card-header d-flex justify-content-center text-light">
        Lavora con noi
      </div>
      <div class="card-body">
        <img class="img-work" src="/media/LavoraConNoi.png" alt="">
        <h5 class="card-text text-light mt-3">Vorresti far parte del nostro team?</h5>
        <p class="card-title text-light">Nessun problema. Clicca qui per diventare un revisore di articoli.</p>
        <div class="d-flex justify-content-center">
          <a id="btn-revisor" href="{{route('revisor.become')}}" class="btn btn-success mt-3">Diventa revisore<i id="icon-check" class="fa fa-check ms-2"></i></a>        
        </div>
      </div>
    </div>
    @endif
    {{-- !SEZIONE LAVORA CON NOI --}}

    <x-divider/>

</x-layout>