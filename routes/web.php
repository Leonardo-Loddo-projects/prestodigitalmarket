<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\RevisorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//PUBLICROUTES
Route::get('/',[PublicController::class,'homePage'])->name('homePage');
Route::get('/creator/team', [PublicController::class, 'team'])->name('creator.team');
Route::get('/ricerca/annuncio', [PublicController::class, 'searchAnnouncements'])->name('announcements.search');

//ANNOUNCEMENTROUTES
Route::get('/announcement/create',[AnnouncementController::class,'create'])->middleware('auth')->name('announcement.create');
Route::get('/announcement/show/{announcement}', [AnnouncementController::class, 'show'])->name('announcement.show');
Route::get('/announcement/index',[AnnouncementController::class,'index'])->name('announcement.index');

//CATEGORYROUTES
Route::get('/{category}/show', [CategoryController::class, 'show'])->name('category.show'); 

//REVISORROUTES
Route::get('/revisor/index', [RevisorController::class,'index'])->middleware('isRevisor')->name('revisor.index');
Route::patch('/accetta/annuncio/{announcement}', [RevisorController::class, 'acceptAnnouncement'])->middleware('isRevisor')->name('revisor.accept_announcement');
Route::patch('/rifiuta/annuncio/{announcement}', [RevisorController::class, 'rejectAnnouncement'])->middleware('isRevisor')->name('revisor.reject_announcement');
Route::get('/revisor/become', [RevisorController::class, 'become'])->middleware('auth')->name('revisor.become');
Route::get('/revisor/make/{user}', [RevisorController::class,'make'])->name('revisor.make');
Route::get('/revisor/restore', [RevisorController::class, 'restoreAnnouncement'])->middleware('isRevisor')->name('revisor.restore');